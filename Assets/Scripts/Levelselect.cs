﻿using UnityEngine;
using System.Collections;

public class Levelselect : MonoBehaviour {

	public static Controller Instance { get; private set; }

	public GameObject CameraNode;
	public GameObject Shot1;
	public Camera Camerafacing;
	private int penguincount;
	private int tigercount;
	public Penguin1Controller penguin1c;
	public TigerControllermenu tiger1c;

		
	// Use this for initialization
	void Start () {
		SetCameraShot (Shot1);
	}
	
	// Update is called once per frame
	void Update () {
		Debug.Log ("PenguinCount: " + penguincount.ToString());
		Debug.Log ("TigerCount: " + tigercount.ToString());
		/*RaycastHit hit;
		Vector3 fwd = transform.TransformDirection(Vector3.forward);
		if (Physics.Raycast (transform.position, fwd, RaycastHit)) 
		{
			Debug.Log (hit.collider.gameObject.name);
		}*/
		RaycastHit hit;
		if (Physics.Raycast (new Ray (CameraNode.transform.position, Camerafacing.transform.rotation * Vector3.forward),
		                    out hit)) 
		{
			if (hit.collider.tag == "tiger")
			{
				tigercount = tigercount + 1;

				if (tigercount == 1)
				{
					tiger1c.DoRoar();
				}

				if (penguincount > 0)
				{
					penguincount = penguincount - 1;
				}
			}
			if (hit.collider.tag == "penguin")
			{
				penguincount = penguincount + 1;

				if (penguincount == 1)
				{
					penguin1c.bunch2();
				}

				if (penguincount == 200)
				{
					Application.LoadLevel ("test");
				}

				if (tigercount > 0)
				{
					tigercount = tigercount - 1;
				}
			}
		
		}
		else 
		{
			if (penguincount > 0)
			{
				penguincount = penguincount - 1;
			}
			if (tigercount > 0)
			{
				tigercount = tigercount - 1;
			}
			animation.enabled=false;
		}
	}
		
	
	void SetCameraShot(GameObject newCamera){
		CameraNode.transform.position = newCamera.transform.position;
		CameraNode.transform.rotation = newCamera.transform.rotation;
	}

}
