﻿using UnityEngine;
using System.Collections;

public class TigerControllermenu : MonoBehaviour {

	public AudioSource TigerSound;

	protected Animator Tiger_Levelselect;

	void Start () {
		Tiger_Levelselect = GetComponent<Animator>();
		Tiger_Levelselect.Play("idle_01", 0, Random.Range(0f, 1f));
	}

	void HandleOnResetToStart (){
	}

	void Update () {
		//if (CompareTag ("roar"))
		//if (animation.IsPlaying("roar"))
			//penguin10sound.Play();
		//else 
			//penguin10sound.Stop();
	}


	public void DoRoar(){
		Tiger_Levelselect.SetBool("bunch2", true);
		Tiger_Levelselect.Play("roar 0", 0, Random.Range(0f, 1f));
		TigerSound.Play();
		}

}
