﻿using UnityEngine;
using System.Collections;
using DG.Tweening;


public class Controller : MonoBehaviour {
	
	public static Controller Instance { get; private set; }
	
	public Material FadeMaterial;
	public GameObject CameraNode;
	public GameObject Shot1, TrackingCamera, Shot3;
	public static event System.Action OnResetToStart;
	public System.Action nextAction;
	private bool _doNextAction = false;
	public bool _useTrackingCamera = false;
	//set so that starts on load
	private bool _waitingToStart = false;
	public Penguin1	penguin1m, penguin2m, penguin3m, penguin5m;
	//these controllers cover physical movement
	public Penguin1Controller penguin1c, penguin2c, penguin1b, penguin2b, penguin3b, penguin4b, penguin5b, penguin6b, penguin7b, penguin8b, penguin9b, penguin10b, penguin11b, penguin12b, penguin13b, penguin14b, penguin15b, penguin16b; 
	//these controllers cover animation
	protected Animator animator;

	
	void Awake()
	{
		if (Instance != null && Instance != this)
		{
			Destroy(gameObject);
		}
		
		Instance = this;
		
		DontDestroyOnLoad(gameObject);
		AudioListener.volume = 0;
	}
	
	void Start(){
		FadeMaterial.color = new Color(0,0,0,1);
		animator = GetComponent<Animator>();
		//attempt to run animator here - move later

		SetupStart();
		Screen.showCursor = false;
		Debug.Log ("waitingtostart-setupstartinitialized");
	}
	
	void OnDestroy(){
		FadeMaterial.color = new Color(0,0,0,0);
	}
	
	void Update(){
		
		if (_waitingToStart){
			if (Input.GetMouseButtonUp(0)){
				_waitingToStart = false;
				
				SetupStart();
				Screen.showCursor = false;
				Debug.Log ("waitingtostart-setupstartinitialized");
			}
		}
		
		if (_doNextAction){
			nextAction.Invoke();
			_doNextAction = false;
		}
		
		AudioListener.volume = 1 - FadeMaterial.color.a;
	}
	
	void LateUpdate(){
		if (_useTrackingCamera){
			SetCameraShot(TrackingCamera);
		}
	}
	
	void SetupStart(){
		if (OnResetToStart != null) OnResetToStart();
		SetCameraShot(Shot1);
		
		_useTrackingCamera = false;
		Debug.Log ("setupshot1firing1");
		FadeInThen(3, RunShot1);
		
	}
	
	void RunShot1(){
		Invoke("FinishShot1", 15);
		//Invoke ("Penguinflop1", 4);
		//Invoke ("Penguinflop2", 4);
		Invoke ("babypeng", 1);
		Invoke ("snore", 6);
		penguin1b.bunch1();
		Invoke ("peng2roar", 2);
		penguin3b.bunch7();
		penguin4b.bunch5();
		penguin5b.bunch4();
		penguin6b.bunch9();
		penguin7b.bunch3();
		penguin8b.bunch6();
		penguin11b.bunch8();
		penguin14b.bunch14();
		penguin15b.bunch15();
		penguin16b.bunch16();
		penguin1c.Still();
		penguin2c.Still();
	}

	void babypeng(){
		penguin9b.bunch3();
	}

	void snore(){
		penguin14b.snore();
	}
	void Penguinflop1(){
		penguin1c.LieDown();
	}
	void Penguinflop2(){
		penguin2c.LieDown();
	}
	void FinishShot1(){
		FadeOutThen(3, SetupShot2);
	}
	
	void SetupShot2(){
		_useTrackingCamera = false;
		SetCameraShot(Shot3);
		Debug.Log ("setupshot2firing1");
		FadeInThen(3, RunShot2);
		//penguin1m.rigidbodystill();
		//penguin2m.rigidbodystill();
		//This was part of an attempt to unlock rigidbody rotation on the penguins as they fell to create more 
		//realistic falling but it just led to them falling all over the place
	}
	
	void RunShot2(){
		Invoke("FinishShot2", 14);
		Invoke("Penguinstartwalk", 17);
		//SlidingPengwalk ();
		penguin3b.Walk2 ();
		penguin3m.walk3 ();
	}
	
	void FinishShot2(){
		FadeOutThen(3, SetupShot3);
	}
	
	void SetupShot3(){
		SetCameraShot(TrackingCamera);
		
		_useTrackingCamera = true;

		FadeInThen(3, RunShot3);
	}

	void Penguinstartwalk(){
		penguin1c.Walk();
		penguin2c.Walk();
	}
	
	void RunShot3(){
		Invoke("FinishShot3", 15);
		Invoke ("peng5move", 5);
		Invoke ("peng2roar", 2);
		//Invoke ("SlidingPengwalk", 7);
		penguin3m.resetposition ();
		penguin1b.bunch1();
		penguin3b.bunch7();
		penguin4b.bunch5();
		penguin5b.bunch4();
		penguin6b.bunch9();
		penguin7b.bunch3();
		penguin8b.bunch6();
		//penguin9b.bunch2();
		//penguin10b.bunch7();
		penguin11b.bunch8();
		//penguin12b.bunch1();
		//penguin13b.bunch9();
		//animations for the crowd, only 9 were used in the end to keep it from slowing down too much, 
		//animations were limited anyway
		penguin14b.bunch14();
		penguin15b.bunch15();
		penguin16b.bunch16();
	}

	void peng2roar(){
		penguin2b.bunch2();
	}

	void peng5move(){
		penguin5b.Walk2 ();
		penguin5m.walk3 ();	
	}

	void SlidingPengwalk(){
		penguin1c.Still();
		penguin2c.Still();
		}

	void FinishShot3(){
		FadeOutThen(3, resetgame);
	}

	void resetgame() {
		Invoke("SetupStart",3);
		penguin1m.resetposition ();
		penguin2m.resetposition ();
		penguin5m.resetposition ();
		}

	void FadeInThen(float duration, System.Action newAction){
		nextAction = newAction;
		FadeMaterial.DOFade(0, duration).OnComplete(FadeFinished);
		_doNextAction = true;
	}
	
	void FadeFinished(){
		_doNextAction = true;
	}
	
	void FadeOutThen(float duration, System.Action newAction){
		nextAction = newAction;
		FadeMaterial.DOFade(1, duration).OnComplete(FadeFinished);
	}
	
	void SetCameraShot(GameObject newCamera){
		CameraNode.transform.position = newCamera.transform.position;
		CameraNode.transform.rotation = newCamera.transform.rotation;
	}
	
	
}
