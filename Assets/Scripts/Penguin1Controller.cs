﻿using UnityEngine;
using System.Collections;

public class Penguin1Controller : MonoBehaviour {

	public AudioSource WalkSound;
	public AudioSource penguinsound1;
	public AudioSource penguinsound2;
	public AudioSource penguinsnore;
	public AudioSource penggullsound;
	public AudioSource penguin10sound;
	public AudioSource footstepssound;
	public AudioSource footstepssound2;
	public Penguin1	penguin1m;
	protected Animator animatorworking;

	void Start () {
		animatorworking = GetComponent<Animator>();
		animatorworking.Play("idle_01", 0, Random.Range(0f, 1f));
	}

	void HandleOnResetToStart (){
	}

	void Update () {
		//if (CompareTag ("roar"))
		//if (animation.IsPlaying("roar"))
			//penguin10sound.Play();
		//else 
			//penguin10sound.Stop();
	}

	public void Walk(){
		//Sets the penguins at the top to walk
		animatorworking.SetBool("isWalking", true);
		animatorworking.SetBool("isSliding", false);  
		animatorworking.SetBool("isStill", false);  
		WalkSound.Play();
		penguin1m.walk1();
		Debug.Log ("animatorfiring1");
		animatorworking.Play("walk", 0, Random.Range(0f, 1f));
		footstepssound2.Play();
	}
	public void Walk2(){
		//Sets the penguins at the top to walk
		animatorworking.SetBool("isWalking", true);
		animatorworking.SetBool("isSliding", false);  
		animatorworking.SetBool("isStill", false);  
		WalkSound.Play();
		penguin1m.walk1();
		Debug.Log ("animatorfiring1");
		animatorworking.Play("walk", 0, Random.Range(0f, 1f));
		footstepssound2.Play();
	}
	public void Still(){
		//Sets the penguins to get up after sliding down the hill and start walking
		animatorworking.SetBool("isWalking", false);
		animatorworking.SetBool("isSliding", false);  
		animatorworking.SetBool("isStill", true);  
		//WalkSound.Play();
		//penguin1m.walk1();
		animatorworking.Play("idle_03 0", 0, Random.Range(0f, 1f));
		//animatorworking.Play("walk", 0, Random.Range(0f, 1f));
	}
	
	public void LieDown(){
		//Sets the penguins to start the sliding animation
		animatorworking.SetBool("isWalking", false);
		animatorworking.SetBool("isSliding", true);  
		animatorworking.SetBool("isStill", false);  
		penguin1m.walk2();
		animatorworking.Play("death", 0, Random.Range(0f, 1f));
	}

	//these control the small animations of the penguins at the bottom of the hill
	public void bunch1(){
		animatorworking.SetBool("bunch1", true);
		animatorworking.Play("butting_heads", 0, Random.Range(0f, 1f));
		penggullsound.Play();
		}
	public void bunch2(){
		animatorworking.SetBool("bunch2", true);
		animatorworking.Play("roar 0", 0, Random.Range(0f, 1f));
		penguin10sound.Play();
		}
	public void bunch3(){
		animatorworking.SetBool("bunch3", true);
		animatorworking.Play("idle_02", 0, Random.Range(0f, 1f));
		animatorworking.SetBool("isWalking", false);
		footstepssound.Play();
		}
	public void bunch4(){
		animatorworking.SetBool("bunch4", true);
		animatorworking.Play("roar", 0, Random.Range(0f, 1f));
		penguinsound1.Play();		
		}
	public void bunch5(){
		animatorworking.SetBool("bunch5", true);
		animatorworking.Play("idle_02 0", 0, Random.Range(0f, 1f));
		}
	public void bunch6(){
		animatorworking.SetBool("bunch6", true);
		animatorworking.Play("surprised", 0, Random.Range(0f, 1f));
		penguinsound2.Play();
		}
	public void bunch7(){
		animatorworking.SetBool("bunch7", true);
		animatorworking.Play("turn_right", 0, Random.Range(0f, 1f));
		}
	public void bunch8(){
		animatorworking.SetBool("bunch8", true);
		animatorworking.Play("idle_03", 0, Random.Range(0f, 1f));
		}
	public void bunch9(){
		animatorworking.SetBool("bunch9", true);
		animatorworking.Play("idle_02 1", 0, Random.Range(0f, 1f));
		}
	public void bunch10(){
		animatorworking.SetBool ("bunch2", true);
		animatorworking.Play ("butting_heads", 0, Random.Range (0f, 1f));
		penggullsound.Play ();
		}
	public void bunch14(){
		animatorworking.SetBool("bunch14", true);
		animatorworking.Play("sleep", 0, Random.Range(0f, 1f));
		//penggullsound.Play();
	}
	public void bunch15(){
		animatorworking.SetBool("bunch9", true);
		animatorworking.Play("idle_02 1", 0, Random.Range(0f, 1f));
	}
	public void bunch16(){
		animatorworking.SetBool("bunch1", true);
		animatorworking.Play("roar 0", 0, Random.Range(0f, 1f));
	}
	public void snore(){
		penguinsnore.Play();
	}
}
