﻿using UnityEngine;
using System.Collections;

public class Penguin1 : MonoBehaviour {
	public Transform target1;
	public Transform target2;
	public Transform target;
	private AudioSource source;
	public Controller controller;
	public float speed = 0;
	public Vector3 startPos;
	
	
	void Start() {
		startPos=transform.position;
	}

	public void rigidbodystill() {
	}
	
	void Update(){
		Vector3 dir = target.position - transform.position;
		// remember, 10 - 5 is 5, so target - position is always your direction.
		
		// magnitude is the total length of a vector.
		// getting the magnitude of the direction gives us the amount left to move
		float dist = dir.magnitude;
		
		// this makes the length of dir 1 so that you can multiply by it.
		dir = dir.normalized;
		
		// the amount we can move this frame
		float move = speed * Time.deltaTime;
		
		// limit our move to what we can travel.
		if(move > dist) move = dist;
		
		// apply the movement to the object.
		transform.Translate( dir * move);
		
		if (transform.rigidbody.velocity == Vector3.zero) {}
		//for walk sound, this was removed because it just sounded terrible and didn't really work
		else {
		}
		
	}
	//moves the penguin in the direction of a target, for some reason this now sends the penguin the opposite 
	//direction of the target, I just worked around this for now.
	public void walk1() {
		target = target1;
		speed = 1;
	}
	public void walk2() {
		target = target2;
		speed = 5;
	}
	public void walk3() {
		target = target2;
		speed = 1;
	}
	public void rigidbodymove() {
		//rigidbody.constraints = RigidbodyConstraints.None;
	}

	public void resetposition() {
		transform.position=startPos;
		speed = 0;
	}
}
