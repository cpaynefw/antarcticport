Pogson Studios - frostbite
--------------------------
Pogson Studios - frostbite - is an alpine quality background mesh package which includes several low poly meshes with high quality textures.  Each mesh is specifically designed as a background object to add a sense of a cold mountainous depth within your scene.


Package Contents
----------------
* 9 low polygon meshs, approx. 2000 triangles per mesh.
* 18 diffuse with matching normal textures.  2 texture styles for each mesh.
* Photoshop file with source masks for each mesh.


Usage Instructions
------------------

* Meshes
Each mesh by default is approximately 100m x 100m in size when placed into a Unity scene.  Each mesh is designed to be used as a background prop in your scene.  You can scale, rotate and move each mesh to suit your scene.

* Photoshop Masks
You can use the source in any way you prefer.  As a typical workflow to get started you would copy your preferred image of a cliff face or rock formation into the layer labelled 'your image here'.  Unhide anyone of the layer groups to expose a default snow texture onto your image.  If required adjust the respective layer opacity settings to suit.


Version History
---------------
version 1.0: Initial version.
version 1.1: Added new example scene (B)



"One or more textures on this 3D-model have been created with images from CGTextures.com. These images may not be redistributed by default. Please visit www.cgtextures.com for more information."