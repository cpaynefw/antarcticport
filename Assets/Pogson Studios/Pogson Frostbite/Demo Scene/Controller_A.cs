﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Controller_A : MonoBehaviour {

	public Transform scene;
	public float rotationSpeed = 5;
	public float cameraSlideSpeed = 5;
	public Transform[] meshList;
	public int distanceBetweenMeshs = 200;


	private Vector3 currentPosition;

	private float upper, lower, sliderRotationSpeed;
	private int currentMeshPos;

	private List<Transform> meshListA = new List<Transform>();

	void Awake()
	{



	}
	
	// Use this for initialization
	void Start () {

		PositionMeshs ();

		upper = 4;
		lower = 0;
		currentMeshPos = 0;
		
		sliderRotationSpeed = 0.6f;
		
		currentPosition = new Vector3 (meshList[currentMeshPos].position.x, transform.position.y, transform.position.z);
		
		transform.position = currentPosition;
	}
	
	// Update is called once per frame
	void Update () {

		// Rotation
		meshList[currentMeshPos].Rotate(Vector3.down,Time.deltaTime * rotationSpeed * sliderRotationSpeed,0);

		PostionChanging();
	}


	void PostionChanging(){
		//transform.currentPosition = currentPosition;

		transform.position = Vector3.Lerp (transform.position, currentPosition, Time.deltaTime * cameraSlideSpeed);
	
	}


	void PositionMeshs(){
		int i = 1;


		foreach (Transform obj in meshList) {
			obj.position = new Vector3(scene.position.x + distanceBetweenMeshs * i, scene.position.y, scene.position.z);
			i++;
		}
	}

	void FillMeshList(){
		foreach (Transform child in scene)
		{
			// do whatever you want with child transform object here
			meshListA.Add(child);
		}
	}


	// GUI
	void OnGUI () {
		
		// Make a background box
		GUI.Box(new Rect(10,10,100,90), meshList[currentMeshPos].transform.name);


		// Make the second button.
		if(GUI.Button(new Rect(20,40,80,20), "Next")) {
			if (currentMeshPos < meshList.Length-1) {
				
				currentMeshPos++;
				
				currentPosition = new Vector3 (meshList[currentMeshPos].position.x, transform.position.y, transform.position.z);
			}
		}
		// Make the first button. If it is pressed, Application.Loadlevel (1) will be executed
		if(GUI.Button(new Rect(20,70,80,20), "Previous")) {
			if (currentMeshPos > 0) {

				currentMeshPos--;

				currentPosition = new Vector3 (meshList[currentMeshPos].position.x, transform.position.y, transform.position.z);
			}
		}


		// Obj Sliders
		sliderRotationSpeed = GUI.HorizontalSlider (new Rect (10, 110, 100, 10), sliderRotationSpeed, lower, upper);

	}
	
}
